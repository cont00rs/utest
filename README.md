# utest

Tiny library for testing in C

## Compilation

Run `make` and `make install`. The static library `libutest.a` is copied to
`~/.local/lib` and the header files into `~/.local/include`. Any other prefix
can be passed too, by setting the `PREFIX` variable, such as `make
PREFIX=/prefix/path install`.

Running `make uninstall` will remove the static library and header files. If a
prefix was set during installation, the same path should be set for `make
uninstall`.

## Usage

1. Define (unit) test subroutines with signature `void test_name(struct utest *ut)`
2. Setup `main` and initialise a test struct `struct utest ut = { 0 }`. For
   floating point comparisons also specify the desired absolute tolerance, e.g.
   `struct utest ut = { .abstol=1e-4 }`.
3. Run all test routines of interest `test_name(&ut)`
4. Report test pass/fail counts: `return ut_report(&ut)`

To test out, a simple example in `main.c` is compiled by `make example`.
Running the resulting program `prog` provides the following test log:

```
[PASS] main.c:test_add:7
[FAIL] main.c:test_add:8    10 == 11
[PASS] main.c:test_multiply:20
[FAIL] main.c:test_multiply:21    3 == 4
[FAIL] main.c:test_multiply:22    64 == 8
[FAIL] main.c:test_float_compare:28    1.500000 == 1.600000, abs: 1.00E-01, rel: 6.67E-02
[FAIL] main.c:test_float_compare:29    1.500000 == 1.510000, abs: 1.00E-02, rel: 6.67E-03
[FAIL] main.c:test_float_compare:30    1.500000 == 1.501000, abs: 1.00E-03, rel: 6.67E-04
[PASS] main.c:test_float_compare:31

[FAIL] pass: 3, fail: 6
```

Success is communicated with return values `EXIT_SUCCESS` and `EXIT_FAILURE`.

## References

Inspired by: [TinyTest](https://github.com/joewalnes/tinytest).
