#include <stddef.h>
#include <math.h>


struct utest {
        unsigned int n_pass;
        unsigned int n_test;
        float abstol;
};

#define ASSERT_EQ(expected, actual) ut_assert(ut, __FILE__, __func__, __LINE__, expected, actual)
#define ASSERT_FLT(expected, actual) ut_assert_flt(ut, __FILE__, __func__, __LINE__, expected, actual)

void ut_assert(struct utest *ut, const char *, const char *, int, int, int);
void ut_assert_flt(struct utest *ut, const char *, const char *, int, float, float);
int ut_report(struct utest *);

#define UT_COLOR_CODE 0x1B
#define UT_COLOR_RED "[1;31m"
#define UT_COLOR_GREEN "[1;32m"
#define UT_COLOR_RESET "[0m"
