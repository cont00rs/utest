#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "utest.h"

void
print_prefix(int pass)
{
        const char *label = pass ? "PASS" : "FAIL";

        if (!isatty(1)) {
                printf("[%s] ", label);
                return;
        }

        const char *color = pass ? UT_COLOR_GREEN : UT_COLOR_RED;
        printf("[%c%s%s%c%s] ", UT_COLOR_CODE, color, label,
               UT_COLOR_CODE, UT_COLOR_RESET);
}

void
increment_counters(struct utest *ut, int pass)
{
        ut->n_test += 1;
        ut->n_pass += pass;
}

void
print_result(struct utest *ut, int pass, const char *filename, const char *funcname, int line)
{
        increment_counters(ut, pass);
        print_prefix(pass);
        printf("%s:%s:%d", filename, funcname, line);
}

void
ut_assert(struct utest *ut, const char *filename, const char *funcname, int line,
          int expected, int actual)
{
        const int pass = expected == actual;
        print_result(ut, pass, filename, funcname, line);
        if (pass) {
                printf("\n");
        } else {
                printf("    %d == %d\n", expected, actual);
        }
}

void
ut_assert_flt(struct utest *ut, const char *filename, const char *funcname, int line,
              float expected, float actual)
{
        const int pass = fabs(actual-expected) <= ut->abstol;
        print_result(ut, pass, filename, funcname, line);
        if (pass) {
                printf("\n");
        } else {
                const float abserr = fabs(actual-expected);
                const float relerr = fabs(abserr/expected);
                printf("    %f == %f, abs: %.2E, rel: %.2E\n", expected, actual, abserr, relerr);
        }
}

int
ut_report(struct utest *ut)
{
        const int ok = ut->n_pass == ut->n_test;
        printf("\n");
        print_prefix(ok);
        printf("pass: %d, fail: %d\n", ut->n_pass, ut->n_test - ut->n_pass);
        return ok ? EXIT_SUCCESS : EXIT_FAILURE;
}
