#include <stdlib.h>
#include "utest.h"

void
test_add(struct utest *ut)
{
        ASSERT_EQ(10, 10);
        ASSERT_EQ(10, 11);
}

int
some_computation(int x)
{
        return x*x*x;
}

void
test_multiply(struct utest *ut)
{
        ASSERT_EQ(2, 2);
        ASSERT_EQ(3, 2*2);
        ASSERT_EQ(64, some_computation(2));
}

void
test_float_compare(struct utest *ut)
{
        ASSERT_FLT(1.5, 1.6);
        ASSERT_FLT(1.5, 1.51);
        ASSERT_FLT(1.5, 1.501);
        ASSERT_FLT(1.5, 1.5001);
}

int
main()
{
        struct utest ut = { .abstol = 1.e-3 };
        test_add(&ut);
        test_multiply(&ut);
        test_float_compare(&ut);
        return ut_report(&ut);
}
