PREFIX ?= ~/.local

CFLAGS = \
	 -Wall \
	 -Wextra \
	 -Wpedantic \
	 -std=c99 \
	 -O3

LDFLAGS = -lm

libutest.a: utest.o
	$(AR) $(ARFLAGS) $@ $<

example: main.c libutest.a
	$(CC) $^ -o prog $(LDFLAGS)

clean:
	$(RM) -r libutest.a utest.o prog

install: libutest.a
	@install -vD libutest.a -t $(PREFIX)/lib
	@install -vD utest.h -t $(PREFIX)/include

uninstall:
	$(RM) $(PREFIX)/lib/libutest.a
	$(RM) $(PREFIX)/include/utest.h

format:
	astyle \
		--align-pointer=name \
		--break-return-type \
		--indent=spaces=8 \
		--style=kr \
		--suffix=none \
		*.c *.h
